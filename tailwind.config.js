
module.exports = {
    content: ["./src/**/*.{html,js}"],
    darkMode: 'class',
    theme: {
      extend: {

        colors: {
          primary: {
              50: '#F3FAF7',
              100: '#DEF7EC',
              200: '#BCF0DA',
              300: '#84E1BC',
              400: '#31C48D',
              500: '#0E9F6E',
              600: '#057A55',
              700: '#046C4E',
              800: '#03543F',
              900: '#014737',
              
              // 950: '#73C1C6'
              950: '#75B9BE'
              // 950: '#157A6E'
          }
        }
    },
    plugins: [],
  }
  }