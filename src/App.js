

import { Footer } from './components/layouts/Footer';
import { Header } from './components/layouts/Header';


import { AppRoutes } from './routes/AppRoutes';

function App() {
  return (
    <div className='dark:bg-slate-800 '>
    <Header />
      <AppRoutes />
    <Footer />
  </div>
  );
}

export default App;
