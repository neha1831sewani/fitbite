/* eslint-disable no-throw-literal */
import { toast } from "react-toastify";
import { getSessionData } from "./authservices";
import { useDispatch } from "react-redux";
import { addToFav } from "../store/favSlice";
import { useAddToFavourites } from "../hooks/useAddToFavourites";


export async function createFavourites(recipe,  user) {
  // console.log(user);
  // console.log("Created");
  // console.log(favList);

  
  const browserData = getSessionData();
  
  const favourites = {
    ...recipe,
    userId: user,
    
    
  };
  // console.log(favourites);
  const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}favourites`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${browserData.token}`,
    },
    body: JSON.stringify(favourites)
  });

  console.log(res);
  if(!res.ok) {
    throw { message: res.statusText, status: res.status};
  }

  const data = await res.json();
 
  return data;
}

export async function getFavourites() {
    const browserData = getSessionData();
    // console.log(browserData); 
    const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}600/favourites?userId=${browserData.userId}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${browserData.token}`,
      },
    });
    
    // console.log(res);
    if(res.status === 404 || res.status === 403) {
      console.log("going");
      return [];
    }
    if(!res) {
      throw { message: res.statusText, status: res.status};
    }
  
    const data = await res.json();
    console.log("get req: : ",data);
    return data;
  } 

  // export async function addFavourites(recipeId, userId) {
  //   const addFavourites = useAddToFavourites(); 
  
    
  //   addFavourites(recipeId, userId);
  // }

  export async function removeFromFavourites(favouriteId, userId) {
    const browserData = getSessionData();
    
    const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}600/favourites/${favouriteId}?userId=${userId}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${browserData.token}`,
      },
    });
  
    if (!res.ok) {
      throw { message: res.statusText, status: res.status };
    }
  
    const data = await res.json();
    return data;
  }
  
  
  