import React from 'react';
import PrivacyPolicyImage from '../assets/cover2.avif';
import { useDynamicTitle } from '../hooks';

export const PrivacyPolicyPage = () => {
  useDynamicTitle("Privacy Policy | Your Recipe Website");
  return (
    <main>
      <div className='privacy-policy-container'>
        <div className='privacy-policy-image-container'>
          <img src={PrivacyPolicyImage} alt="privacy-policy"  />
       
        </div>
        <div className=' dark:text-slate-200'>
          <h2 className='h2 mt-3 mb-3 text-2xl'>Privacy Policy for <strong> FITBITE</strong></h2>
          <p className='mb-4'>
            Last Updated: 26th Dec 2023
          </p>
          <p className='mb-4'>
            Thank you for using <strong>FITBITE</strong>. Your privacy is important to us, and we are committed to protecting your personal information. This Privacy Policy explains how we collect, use, disclose, and safeguard your information when you use our website and related services (collectively, the "Services").
            By using our Services, you consent to the practices described in this Privacy Policy. If you have any questions or concerns about our Privacy Policy, please contact us at fitbite@gmail.com.
          </p>
          <h2 className='h2 text-lg'>CONSENT</h2>
          <p className='mb-4'>By using our Services, you hereby consent to our Privacy Policy and agree to its terms.</p>
          <h2 className='h2 text-lg'> INFORMATION WE COLLECT</h2>
          <p className='mb-4'>We collect both personal and non-personal information. Personal information includes, but is not limited to, your name, email address, and contact information. Non-personal information includes technical details such as IP address, browser type, and device information.</p>
          <h2 className='h2 text-lg'>HOW WE USE YOUR INFORMATION</h2>
          <ul className='mb-4  '>
            <li><span className='bi bi-check2'></span> To provide recipes, restaurant information, and meal planning services.</li>
            <li><span className='bi bi-check2'></span> To personalize and improve your user experience.</li>
            <li><span className='bi bi-check2'></span> To process your account registration and manage your preferences.</li>
            <li><span className='bi bi-check2'></span> To send you relevant updates, newsletters, and promotional materials.</li>
            <li><span className='bi bi-check2'></span> To analyze user behavior and improve our website and services.</li>
            <li><span className='bi bi-check2'></span> To comply with legal obligations.</li>
          </ul>
          <h2 className='h2 text-lg'>COOKIES AND WEB BEACONS</h2>
          <p className='mb-4'>We use cookies and web beacons to enhance your experience on our website. These technologies help us collect information about your preferences, navigate effectively, and understand usage patterns.</p>
          <h2 className='h2 text-lg'>ADVERTISING PARTNERS PRIVACY POLICIES</h2>
          <p className='mb-4'>Our advertising partners may use cookies and similar technologies to collect and track information for advertising purposes. Please refer to their privacy policies for more information on how they handle your data.</p>
          <h2 className='h2 text-lg'>THIRD PARTY PRIVACY POLICIES</h2>
          <p className='mb-4'>Our Services may contain links to third-party websites. We have no control over the privacy practices and content of these websites. Please review the privacy policies of these third parties for more information.</p>
          <h2 className='h2 text-lg'>GDPR DATA PROTECTION RIGHTS</h2>
          <p className='mb-4'>If you are a resident of the European Economic Area (EEA), you have certain data protection rights. These include the right to access, correct, delete, or restrict the processing of your personal information.</p>
          <h2 className='h2 text-lg'>CHILDREN'S INFORMATION</h2>
          <p className='mb-4'>Our Services are not directed to children under the age of 13. We do not knowingly collect or solicit personal information from children. If you believe that we may have collected personal information from a child, please contact us immediately.</p>
          <h2 className='h2 text-lg'>UPDATES TO THIS POLICY</h2>
          <p className='mb-4'>We reserve the right to update or change our Privacy Policy at any time. We will notify you of any changes by posting the new Privacy Policy on this page.</p>
        </div>
      </div>
    </main>
  );
};
