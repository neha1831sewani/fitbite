/* eslint-disable no-throw-literal */
import Cookies from "js-cookie";

export async function getUserRegister (authDetails) {
    const authOptions = {
        method : 'POST',
        headers: {
            'Content-type': 'application/json'
        }, 
        body:JSON.stringify(authDetails),
    };
    const URL = `${process.env.REACT_APP_BACKEND_URL}register`
    // console.log(authOptions)
  
    const response = await fetch(URL, authOptions);

    if(!response.ok) {
        const errorObj = {
            message: response.statusText,
            status: response.status,
        }
        throw new Error(JSON.stringify(errorObj));
    }

    const data = await response.json();
    // console.log(data)

    return data;
}


export async function getUserLogin (authDetails) {
    const authOptions = {
        method : 'POST',
        headers: {
            'Content-Type': 'application/json'
        }, 
        body:JSON.stringify(authDetails),
    };
    const URL = `${process.env.REACT_APP_BACKEND_URL}login`
    const response = await fetch(URL, authOptions);

    if(!response.ok) {
        const errorObj = {
            message: response.statusText,
            status: response.status,
        }
        throw new Error(JSON.stringify(errorObj));
    }

    const data = await response.json();
    // console.log(data)
    return data;

}

export function isUserLoggedIn(){
    const token = Cookies.get('accessToken')
   if(token) {
        return true;
   }
   return false;

}


export async  function getLoggedInUserDetails() {
    if(isUserLoggedIn()) {
      const token = Cookies.get('accessToken');
      const userId = Cookies.get('id');
      const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}600/users/${userId}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`
        }
      });
  
      if(!res) {
        throw {message: res.statusText, status: res.status};
      }
  
      const data = await res.json();
      // console.log(data)
      return data;
    }
  }
  
  export function getSessionData() {
    const token = Cookies.get('accessToken');
    const userId = Cookies.get('id');
    return {
      token, 
      userId
    };
  }
  


  export function logout() {
    Cookies.remove('accessToken');  
    Cookies.remove('id');
    Cookies.remove('name');  
    Cookies.remove('email');
    // Cookies.remove('favourites');
  }
  
