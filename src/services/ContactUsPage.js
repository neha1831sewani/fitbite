import React from 'react'

export const ContactUsPage = () => {
  return (
    <main>
      <div className='dark:text-slate-100'>
      <p className='font-bold text-2xl mt-2'>ContactUs</p>
     
<p className='mt-1 text-xl' >Do you have any questions, suggestions, feature requests, or want to collaborate? We would love to hear from you! <span ><a href="/" className='underline text-blue-700 '>Please email us.</a></span>
</p>

    </div>
    </main>
  )
}
