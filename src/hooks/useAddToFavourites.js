// useAddFavourites.js

import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { addToFav } from '../store/favSlice';
import { createFavourites, getFavourites } from '../services/favService';
import { toast } from 'react-toastify';

export const useAddToFavourites = () => {
//   const dispatch = useDispatch();

  const addFavourites = async (recipe, userId) => {
    try {
      console.log("called");
    //   const data = await getFavourites();
    //   console.log(data);
    //   const newData = data.concat(recipeId);
    //   console.log(newData);
      await createFavourites(recipe, userId); 
    //   dispatch(addToFav({ recipe}));
    } catch(error) {
      toast.error(error.message, {
        position: toast.POSITION.BOTTOM_RIGHT
      });
    }
  };

  return addFavourites;
};
