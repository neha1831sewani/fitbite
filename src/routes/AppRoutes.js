import React from 'react'
import { Route, Routes } from 'react-router-dom'
import { HomePage } from '../pages/Home/HomePage'
import { MealPlanner } from '../pages/MealPlanner/MealPlanner'

import { AboutUsPage } from '../pages/About-us/AboutUsPage'
import { ContactUsPage } from '../services/ContactUsPage'
import { PrivacyPolicyPage } from '../services/PrivacyPolicyPage'
import { Register } from '../pages/Auth/Register'
import { Login } from '../pages/Auth/Login'

import { RecipesPage } from '../pages/Recipies/RecipesPage'
import { RecipeDetailsPage } from '../pages/Recipies/RecipeDetailsPage'
import { SearchPage } from '../pages/SearchPage'
import { RestaurantsPage } from '../pages/Restaurants/RestaurantsPage'

import { TermsOfServicePage } from '../pages/TermsOfServicePage'
import { SearchByIngredients } from '../pages/SearchByIngredients'
import { FavouritesPage } from '../pages/Favourites/FavouritesPage'
import { ProtectedRoute } from './ProtectedRoute'



export const AppRoutes = () => {
  return (
   <>
        <Routes>
            <Route path ="/" element = { <HomePage />} />
            {/* <Route path='/products' element={ <HomePage /> } /> */}
            <Route path ="/recipes" element = {<RecipesPage />} />
            <Route path ="/recipes/:id" element = { <RecipeDetailsPage />} />
            <Route path ="/meal-planner" element = { <MealPlanner />} />
            <Route path ="/about-us" element = { <AboutUsPage/>} />
            <Route path ="/restaurants" element = { <RestaurantsPage/>} />

            <Route path ="/contact-us" element = { <ContactUsPage />} />
            <Route path ="/privacy-policy" element = { <PrivacyPolicyPage />} />
            <Route path ="/terms-of-service" element = { <TermsOfServicePage />} />
            <Route path ="/register" element = { <Register />} />
            <Route path ="/login" element = { <Login />} />
            <Route path ="/recipes/search" element = { <SearchPage />} />
            <Route path ="/recipes/ingredients" element = { <SearchByIngredients />} />
            <Route path ="/favourites" element = {  <FavouritesPage />} />
           
      


        </Routes>
    
   </>
  )
}
