import React from 'react'

import { Navigate } from 'react-router-dom';
import { getSessionData } from '../services/authservices';

export const ProtectedRoute = ({ children, path, element }) => {
  const browserData = getSessionData();
  return browserData.token ?  children  : <Navigate to='/login' />
}

