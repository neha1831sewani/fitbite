import React from 'react'

import { WhyUs } from './components/WhyUs'
import { OurVision} from './components/OurVision'
import { ServicesProvided } from './components/ServicesProvided'
import { useDynamicTitle } from '../../hooks'
import { HeroSection } from './components/HeroSection'


export const HomePage = () => {
  useDynamicTitle('Home | Fitbite' , "Fitbite");
  return (
  
    <>
    {/* <div className='mb-16'>
     
    </div> */}
    
     <main>
     <HeroSection />
        <WhyUs />
        <OurVision/>
        <ServicesProvided />
    </main>
    
    
    </>
  
  )
}
