import React from 'react'
import { Link } from 'react-router-dom'
import { Button } from '../../../components/elements/Button'



export const HeroSection = () => {
  // const heroImagePath = '../../../assets/heroimage.avif';
  return (
    <div>


<section class="bg-center bg-no-repeat heroImage mt-2 mb-8 bg-gray-700 bg-blend-multiply">
    <div class="px-4 mx-auto max-w-screen-xl text-center py-24 lg:py-56">
        <h1 class="mb-4 text-4xl font-extrabold tracking-tight leading-none text-white md:text-5xl lg:text-6xl">Welcome to Fitbite</h1>
        <p class="mb-8 text-lg font-normal text-gray-300 lg:text-xl sm:px-16 lg:px-48">At Fitbite, we believe in the power of nutrition to transform lives. Our commitment is to provide you with the knowledge, tools, and inspiration you need to make informed choices about your health and well-being.</p>
        <div class="flex flex-col space-y-4 sm:flex-row sm:justify-center sm:space-y-0">
            <Link href="/recipes" class="">
               <Button>
               Get started
                <svg class="w-3.5 h-3.5 ms-2 rtl:rotate-180" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 10">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 5h12m0 0L9 1m4 4L9 9"/>
                </svg>
               </Button>
            </Link>
            
        </div>
    </div>
</section>


    </div>
  )
}
