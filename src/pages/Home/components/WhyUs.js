import React from "react";
import culinary from "../../../assets/culinary.avif";
import nutritional from "../../../assets/nutritional.avif";
import quicksoln from "../../../assets/quickcooking.avif";

export const WhyUs = () => {
  return (
    <main>
      <div className="container mx-auto dark:text-white mb-8 ">
        <h2 className="text-4xl font-medium mb-4 text-center sectionTitle">
          Why Choose Us?
        </h2>
        <div className="flex flex-wrap justify-start">
          <div className="mx-auto w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
            <div className="h-72">
              <img
                className="rounded-t-lg w-[100%] h-[100%] object-cover"
                src={culinary}
                alt=""
              />
            </div>
            <div class="p-5">
              <a href="/">
                <h5 class="mb-2 text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                  Culinary Diversity Beyond Borders:
                </h5>
              </a>
              <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">
                Embark on a global gastronomic journey with recipes inspired by
                cuisines from around the world. Experience the richness of
                diverse flavors and expand your culinary horizons.
              </p>
            </div>
          </div>
          <div className="mx-auto w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
            <div className="h-72">
              <img
                className="rounded-t-lg w-[100%] h-[100%] object-cover"
                src={nutritional}
                alt=""
              />
            </div>
            <div class="p-5">
              <a href="/">
                <h5 class="mb-2 text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                  Nutritional Excellence
                </h5>
              </a>
              <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">
                Prioritize health without compromising on flavor with our
                nutritionally balanced recipes. Discover meals crafted to meet
                dietary needs and promote overall well-being.
              </p>
            </div>
          </div>
          <div className="mx-auto w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
            <div className="h-72">
              <img
                className="rounded-t-lg w-[100%] h-[100%] object-cover"
                src={quicksoln}
                alt=""
              />
            </div>
            <div class="p-5">
              <a href="/">
                <h5 class="mb-2 text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                  Time-Saving Solutions
                </h5>
              </a>
              <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">
                Bid farewell to mealtime stress with recipes designed for
                efficiency without sacrificing quality. Spend less time in the
                kitchen and more time savoring delicious meals.
              </p>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
};
