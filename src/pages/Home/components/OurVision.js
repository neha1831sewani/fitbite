import React from "react";
import { Link } from "react-router-dom";
import HeroImage from "../../../assets/cover1.avif";
import { Button } from "../../../components/elements/Button";


export const OurVision = () => {
  return (
    <section className="bg-white mx-auto  mb-8 py-4 px-8 dark:bg-slate-800 flex justify-between items-center">
      <div className="lg:py-16">
        <h1 className="text-4xl font-medium mb-4 dark:text-white  ">
          Our Vision
        </h1>
        <p className="mb-2 text-lg font-normal text-gray-500 dark:text-gray-400 text-justify">
        
        At <span className="font-extrabold">Fitbite</span>, our vision extends beyond just recipes and meal planning. We aspire to foster a holistic approach to health , envisioning a future where individuals are empowered to embrace a nourished and vibrant lifestyle. We passionately believe that every meal can be a transformative experience, shaping not only physical health and promoting mental and emotional well-being.

In our pursuit of a healthier tomorrow, we strive to create a community that celebrates the joy of cooking, values the connection between food and life, and prioritizes mindful choices. Our vision is grounded in the belief that small, positive changes in daily routines can lead to long-lasting benefits.
Join us on this journey towards a revitalized life, where each recipe and meal plan is a step towards a harmonious relationship with food.


        </p>
        <div className="flex flex-col space-y-4 sm:flex-row mb-8  sm:space-y-0">
          <Link to="/about-us">
            <Button>
             About Us
              <svg
                className="w-3.5 h-3.5 ms-2 rtl:rotate-180"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 14 10"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M1 5h12m0 0L9 1m4 4L9 9"
                />
              </svg>
            </Button>
          </Link>
        </div>
      </div>
      <div className="ml-5">
        <img src={HeroImage} alt="Plant" className="rounded-lg " />
      </div>
    </section>
  );
};

