import React from "react";


export const SliderCard = ({ src }) => {
  return (
    <figure className=" h-[530px]  mt-[0.7px] w-full m-auto text-center  ">
        <img src={ src }  className= "h-full w-full object-cover" alt="hi" />
     
      <figcaption className="flex items-center justify-center mt-6 space-x-3 rtl:space-x-reverse">
        
      </figcaption>
    </figure>
  );
};

