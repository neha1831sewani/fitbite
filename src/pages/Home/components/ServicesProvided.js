import React from "react";
import recipesimage from "../../../assets/recipesimage.avif";
import nearbyrestaurant from "../../../assets/nearbyrestaurant.avif";
import foodplanner from "../../../assets/foodplanner.avif";

export const ServicesProvided = () => {
  return (
   
      <div className="container  mx-auto dark:text-white mb-2">
        <h2 className="text-4xl font-medium mb-4 text-center sectionTitle">
         Services Provided
        </h2>
        <div className="flex flex-wrap justify-start">
          <div className="mx-auto w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
            <div className="h-72">
              <img
                className="rounded-t-lg w-[100%] h-[100%] object-cover"
                src={recipesimage}
                alt=""
              />
            </div>
            <div class="p-5">
              <a href="/">
                <h5 class="mb-2 text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                Recipes Galore
                </h5>
              </a>
              <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">
              From Novice to Gourmet: Access a diverse collection of recipes suitable for every skill level. Detailed instructions, nutritional information, and user reviews make every cooking experience enjoyable and rewarding.
              </p>
            </div>
          </div>
          <div className="mx-auto w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
            <div className="h-72">
              <img
                className="rounded-t-lg w-[100%] h-[100%] object-cover"
                src={foodplanner}
                alt=""
              />
            </div>
            <div class="p-5">
              <a href="/">
                <h5 class="mb-2 text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                Meal Planner
                </h5>
              </a>
              <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">
              Tailored to Your Taste: Our meal planner creates personalized plans based on your dietary preferences, restrictions, and schedule. Effortlessly plan your meals for the week and streamline your grocery shopping.
              </p>
            </div>
          </div>
          <div className="mx-auto w-full max-w-sm  border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 bg-white">
            <div className="h-72">
              <img
                className="rounded-t-lg w-[100%] h-[100%] object-cover"
                src={nearbyrestaurant}
                alt=""
              />
            </div>
            <div class="p-5">
              <a href="/">
                <h5 class="mb-2 text-xl font-bold tracking-tight text-gray-900 dark:text-white">
                Nearby Restaurants
                </h5>
              </a>
              <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">
              Beyond Your Kitchen Doors: Discover local culinary gems with our restaurant recommendations. Filter by cuisine, rating, and proximity to find the perfect dining spot for any occasion.
              </p>
            </div>
          </div>
        </div>
      </div>
   
  );
};
