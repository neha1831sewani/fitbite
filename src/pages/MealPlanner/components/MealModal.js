import React, { useEffect, useRef } from 'react'
import { Link } from 'react-router-dom'

export const MealModal = ({isModalVisible, onModalClose, onAddMeal}) => {

    const recipeRef = useRef('');
    const timeRef = useRef('');

    const onHandleSubmit = (evt) => {
        evt.preventDefault();
        if (recipeRef.current.value === '') {
            alert("Please add the meal name you want to eat!");
            return;
        }
    
        if (timeRef.current.value === 'Select time of the day') {
            alert("Please select the part of the day you would like to add your meal for!");
            return;
        }

        const mealToBeAdded = {
            name: recipeRef.current.value,
            status: timeRef.current.value

        }
    

        onAddMeal(mealToBeAdded)
           recipeRef.current.value = '';
           timeRef.current.value = '';
          
     
    }
  return (
    
<div className={`fixed top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 ${isModalVisible ? "" : "hidden"} border-2 rounded-lg border-sky-700 `}>
<div id="authentication-modal" tabindex="-1" aria-hidden="true" className={` overflow-y-auto overflow-x-hidden  z-50 justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] max-h-full`}>
    <div class="relative   w-full max-w-md max-h-full">
       
       <form action="" onSubmit={onHandleSubmit}>
       <div class="relative bg-blue-200 rounded-lg shadow dark:bg-gray-700">
           
           <div class="flex items-center justify-between  md:p-5 border-b rounded-t dark:border-gray-600">
               <h3 class="text-xl font-semibold text-gray-900 dark:text-white">
                   Schedule Meal
               </h3>
               <button type="button" class="end-2.5 text-gray-600 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ms-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white" data-modal-hide="authentication-modal"
               onClick={onModalClose}>
                   <svg class="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14">
                       <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"/>
                   </svg>
                   <span class="sr-only">Close modal</span>
               </button>
           </div>
          
           <div class="p-4 md:p-5">
               <form class="space-y-4" action="#">
                   <div>
                       <label for="Meal" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Recipe Name</label>
                       <input  class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" placeholder="Burger and Fries" required 
                       ref={recipeRef}/>
                   </div>
                   <div>
   
                       <div class="col-span-2 sm:col-span-1">
                       <label for="" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Time of the day</label>
                       <select id="" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-3 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white  dark:focus:border-primary-500"
                       ref={timeRef}>
                           <option selected="">Select time of the day</option>
                           <option value="Breakfast">Breakfast</option>
                           <option value="Lunch">Lunch</option>
                           <option value="Dinner">Dinner</option>
                       </select>
                   </div>
                   </div>
                  
                   <button type="submit" class="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                   onClick={onHandleSubmit}>Add to Schedule</button>
                   
               </form>
           </div>
       </div>

       </form>
       
                </div>
            </div>
        </div>


  )
}
