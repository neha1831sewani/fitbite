

export const MealPlannerHeader = ({onModalOpen}) => {
   
  return (
   
      <>
      <div class="max-w-screen-xl mt-4  flex flex-wrap items-center justify-between mx-auto p-4">
        <div class="flex md:order-2 space-x-3 md:space-x-0 rtl:space-x-reverse">
          <button
            type="button"
            class="text-white bg-blue-700 hover:bg-blue-800focus:outline-none font-medium rounded-lg text-sm px-4 py-2 text-center dark:bg-blue-600 dark:hover:bg-blue-700"
            onClick={ onModalOpen }
          >
            <span className="text-2xl font-bold">+</span>
            
          </button>
          <button
            data-collapse-toggle="navbar-cta"
            type="button"
            class="inline-flex items-center p-2 w-10 h-10 justify-center text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none dark:text-gray-400 dark:hover:bg-gray-700 "
            aria-controls="navbar-cta"
            aria-expanded="false"
          >
            <span class="sr-only">Open main menu</span>
            <svg
              class="w-5 h-5"
              aria-hidden="true"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 17 14"
            >
              <path
                stroke="currentColor"
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M1 1h15M1 7h15M1 13h15"
              />
            </svg>
          </button>
        </div>
        <div
          class="items-center justify-between w-full md:flex md:w-auto md:order-1"
          id="navbar-cta"
        >
          <i className="bi bi-calendar2-day dark:text-white text-2xl mr-2"></i>

          <h2 className="text-2xl font-medium dark:text-white ">
            Meal Planner with Grocery List, Food Tracker, Recipes, and Products
          </h2>
        </div>
      </div>

      
      </>
   


  );
};
