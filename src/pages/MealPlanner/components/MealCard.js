import React from 'react';
import { Link } from 'react-router-dom';

export const MealCard = ({ meal }) => {
  const { name, status } = meal;

  
  return (
    <div className="flex flex-col items-center bg-white border border-gray-200 rounded-lg shadow-md hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700 transition-all mb-4 w-full dark:text-white ">
      <div className="flex flex-col justify-between p-4 leading-normal w-full">
        <div className=" flex items-start justify-between">
          <div className="flex items-center">
            <div className="text-2xl font-semibold text-gray-900 dark:text-white mb-2">{name}</div>
            
          </div>
         
        </div>

       

            <div class="flex justify-between mt-2">
            <div>
            <p className=" text-lg mb-2">
              Want to see related recipes?

              {/* https://api.spoonacular.com/recipes/findByIngredients */}
              <Link to={`/recipes/ingredients/?q=${name}`} className='text-blue-700 decoration-solid underline'>Click Here</Link>
            </p>
            </div>
                       <div>
                       <button type="button" className="text-red-500 hover:text-red-600 bg-red-200 py-[0.2rem] px-1.5  border-2   mr-3 rounded  border-red-700 focus:outline-none">
                            <i class="bi-trash-fill">
                            <span class="sr-only">Delete</span>
                            </i>
                        </button>
                        <button type="button" class="text-yellow-500 hover:text-yellow-600 
                        bg-yellow-200 py-[0.2rem] px-1.5  rounded  border-2 border-amber-500 focus:outline-none">
                            <i class="bi-pencil-fill"></i> 
                            <span class="sr-only">Edit</span>
                        </button>
                       </div>
                    </div>
        <div className="flex items-center text-gray-500 dark:text-gray-400">
          <i className="bi-clock mr-1"></i>
          <span>{status}</span>
        </div>
        
      </div>
    </div>
  );
};
