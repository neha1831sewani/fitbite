import React, { useEffect, useState } from 'react'
import { MealPlannerHeader } from './components/MealPlannerHeader'
import { v4 as uuidv4 } from "uuid";
import { MealModal } from './components/MealModal';
import { MealCard } from './components/MealCard';


export const MealPlanner = () => {
  const [isMealModalVisible, setMealModal] = useState(false);
  const [meals, setMeals] = useState(
    JSON.parse(localStorage.getItem("meals")) || []
  );
  

  const onModalOpen = () => {
      setMealModal(true);

  }

  const onModalClose = () => {
      console.log("called");
      setMealModal(false);
  }
  

  const handleAddMeal = (meal) => {

    const mealToBeAdded = {
      ...meal,
      id: uuidv4(),

      
    };
    setMeals([...meals, mealToBeAdded]);
    setMealModal(false);
  };

  useEffect(() => {
    localStorage.setItem("meals", JSON.stringify(meals));
  }, [meals]);

  return (
   <>
    <main>
    <MealPlannerHeader  onModalOpen = {onModalOpen}/>

{meals &&  meals.map((meal) => (
      <MealCard
        key={meal.id}
        meal={meal}
       
      />
    ))}
<MealModal  isModalVisible = { isMealModalVisible } onModalClose = { onModalClose } onAddMeal={handleAddMeal}/>

    </main>
    </>

  )
}
