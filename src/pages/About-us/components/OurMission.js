import React from "react";
import { Link } from "react-router-dom";
import HeroImage from "../../../assets/cover5.avif";
import { Button } from "../../../components/elements/Button";


export const OurMission = () => {
  return (
    
    <section className=" mx-auto   py-4 px-8 dark:bg-slate-800 flex justify-between items-center">
        
      <div className="lg:py-16">
        <h1 className="text-4xl font-medium mb-4 dark:text-white  ">
          Our Mission
        </h1>
        <p className="mb-2 text-lg font-normal text-gray-500 dark:text-gray-400 text-justify">
        
        As a  <span className="font-extrabold">Fitbite</span> user, you can already add your favorite recipes and store bought products to our free meal planner, which automatically generates your shopping list and calculates the nutritional information for you. Whether you're cooking from scratch or picking something up at the store, our meal planner doubles as a food tracker that counts your calories, protein, fat, carbs, sugar, and other nutrients for you.

        With our food search engine, you will soon find everything from "protein shake with 20 grams of protein" to "best vegan restaurant in Chicago" to "Paleo brownie recipes."

        We want to make Fitbite the only site you need when it comes to the food you eat.

        <span className="font-bold">  All your food. One place.</span>

        </p>
        <div className="flex flex-col space-y-4 sm:flex-row  sm:space-y-0">
          <Link to="/about-us">
            <Button>
             About Us
              <svg
                className="w-3.5 h-3.5 ms-2 rtl:rotate-180"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 14 10"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M1 5h12m0 0L9 1m4 4L9 9"
                />
              </svg>
            </Button>
          </Link>
        </div>
      </div>
      <div className="ml-5">
        <img src={HeroImage} alt="Plant" className="rounded-lg " />
      </div>
    </section>
  );
};

