import React from 'react'
import nutritionist from '../../../assets/nutritionist.avif'
import fitnesscoach from '../../../assets/fitnesscoach.avif'
import chef from '../../../assets/chef.avif'

export const WhoWeAre = () => {
  return (
    <div className='py-4 px-8 '>
         <h1 className="text-4xl  text-center font-medium mb-4 dark:text-white  ">
          Who We Are?
        </h1>
        
        <div >
       
        <div className=' text-lg dark:text-slate-200'>Meet the passionate minds behind Fitbite. Our team comprises experienced nutritionists, dietitians, fitness enthusiasts, and wellness experts. We share a common goal: to guide you on your journey to a healthier, happier you.</div>
        </div>

        <div className=' flex items-center justify-start mt-4'>
            
        

<div class="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 mr-4 h-[560px]">
    <a href="/">
        <img class="rounded-t-lg" src={nutritionist} alt="" className='w-[100%] h-[50%] object-cover' />
    </a>
    <div class="p-5">
        <a href="/">
            <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">The Nutrition Enthusiast:</h5>
        </a>
        <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">Meet Sarah, our lead nutritionist and the driving force behind Fitbite. With a background in dietetics and a passion for whole-food nutrition, Sarah is dedicated to translating complex nutritional science into practical advice. She believes in the power of food as medicine and is committed to helping you achieve your health goals through personalized guidance.</p>
       
    </div>
</div>


<div class="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 mr-4 h-[560px]">
    <a href="/">
        <img class="rounded-t-lg" src={chef} alt=""className='w-[100%] h-[50%] object-cover' />
    </a>
    <div class="p-5">
        <a href="/">
            <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">The Culinary Maven:</h5>
        </a>
        <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">Introducing Chef Mia, our culinary maestro. With a background in culinary arts and a flair for creating delicious, nutritious meals, Mia is on a mission to make healthy eating an enjoyable experience. Follow her tips for incorporating vibrant, flavorful dishes into your routine, and discover how to savor the journey to a healthier you.</p>
       
    </div>
</div>

<div class="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 mr-4 h-[560px]">
    <a href="/">
        <img class="rounded-t-lg" src={fitnesscoach} alt="" className='w-[100%] h-[50%] object-cover' />
    </a>
    <div class="p-5">
        <a href="/">
            <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">The Fitness Guru:</h5>
        </a>
        <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">Say hello to Alex, our fitness and wellness expert. As a certified personal trainer and nutrition coach, Alex brings a unique blend of expertise to Fitbite. With a focus on the symbiotic relationship between nutrition and fitness, Alex is here to guide you in creating a balanced lifestyle that supports your physical and mental well-being.</p>
       
    </div>
</div>

        </div>
        
   
        
    </div>
  )
}
