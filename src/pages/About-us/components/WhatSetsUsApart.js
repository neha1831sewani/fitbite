import React from 'react';
import expertise from '../../../assets/expertise.avif'
import holistic from '../../../assets/holsitic.avif'
import community from '../../../assets/community.avif'
import nnovative from '../../../assets/nnovative.avif'

export const WhatSetsUsApart = () => {
  return (
    <section className=" py-16  dark:text-white">
      <div className="container mx-auto text-center">
        <h1 className="text-4xl font-medium mb-4 dark:text-white">What Sets Us Apart</h1>
        <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 gap-8">
          {/* Feature 1 */}
          <div className="flex flex-col items-center">
            <img
              src={expertise}
              alt="Feature 1 Icon"
              className=" mb-4"
            />
            <h3 className="text-xl font-bold mb-2">Expertise</h3>
            <p className="text-gray-700 text-lg text-justify dark:text-gray-400">
              Our team consists of certified nutritionists and experts with years of experience, ensuring you receive the highest level of expertise.
            </p>
          </div>

          {/* Feature 2 */}
          <div className="flex flex-col items-center">
            <img
              src={holistic}
              alt="Feature 2 Icon"
              className=" mb-4"
            />
            <h3 className="text-xl font-bold mb-2">Holistic Approach</h3>
            <p className="text-gray-700 text-lg text-justify dark:text-gray-400">
         
              We believe in a holistic approach to health, addressing both nutritional choices and overall well-being for a comprehensive lifestyle change.
            </p>
          </div>

          {/* Feature 3 */}
          <div className="flex flex-col items-center">
            <img
              src={community}
              alt="Feature 3 Icon"
              className=" mb-4"
            />
            <h3 className="text-xl font-bold mb-2">Community Engagement</h3>
            <p className="text-gray-700 text-lg text-justify dark:text-gray-400">
         
              Join our vibrant community to connect with like-minded individuals, share experiences, and find support on your health journey.
            </p>
          </div>

          {/* Feature 4 */}
          <div className="flex flex-col items-center">
            <img
              src={nnovative}
              alt="Feature 4 Icon"
              className=" mb-4"
            />
            <h3 className="text-xl font-bold mb-2">Innovative Resources</h3>
            <p className="text-gray-700 text-lg text-justify dark:text-gray-400">
              Explore our innovative resources designed to make your health journey seamless and enjoyable, including our mobile app and interactive tools.
            </p>
          </div>
        </div>
      </div>
    </section>
  );
};


