import React from 'react'
// import Slider from 'react-slick';
import { SliderCard } from '../../Home/components/SliderCard';
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import { useFetch } from '../../../hooks';
import Slider from 'react-slick';
import { cover1, cover2, cover3, cover4, cover5 } from '../../../Helper';
import { Link } from 'react-router-dom';


const images = [ cover1, cover2, cover3, cover4, cover5]

export const HeroSection = () => {
  
  return (
   
  
<main>
<section
    class="relative bg-[url(https://images.unsplash.com/photo-1556909211-36987daf7b4d?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D)] bg-cover bg-center bg-no-repeat mt-8  opacity-60"
    
  >
    <div
      class="absolute inset-0 bg-white/75 sm:bg-transparent sm:from-white/95 sm:to-white/25 ltr:sm:bg-gradient-to-r rtl:sm:bg-gradient-to-l "
    ></div>
  
    <div
      class="relative mx-auto max-w-screen-xl px-4 py-32 sm:px-6 lg:flex lg:h-screen lg:items-center lg:px-8"
    >
      <div class="max-w-xl text-center ltr:sm:text-left rtl:sm:text-right">
        <h1 class="text-3xl  font-extrabold sm:text-5xl">
          Let us find your
  
          <strong class="block font-extrabold text-blue-700"> Healthy recipes </strong>
        </h1>
  
        <p class="mt-4 max-w-lg sm:text-xl/relaxed">
        At FitBite, we believe in the power of nourishing food to fuel a healthy lifestyle. Our mission is to inspire and empower individuals on their journey towards wellness through delicious, nutritious recipes and helpful resources
        </p>
  
        <div class="mt-8 flex flex-wrap gap-4 text-center">
          <Link
            to={'/recipes'}
            class="block w-full rounded bg-blue-600 px-12 py-3 text-sm font-medium text-white shadow hover:bg-blue-400 focus:outline-none focus:ring active:bg-blue-500 sm:w-auto"
          >
            Get Started
          </Link>
  
          <Link
            to={''}
            class="block w-full rounded bg-white px-12 py-3 text-sm font-medium text-blue-600 shadow hover:text-blue-700 focus:outline-none focus:ring active:text-blue-500 sm:w-auto"
          >
            Learn More
          </Link>
        </div>
      </div>
    </div>
  </section>
</main>
  )
}
