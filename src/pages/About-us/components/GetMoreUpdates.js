import React from 'react'
import { Link } from 'react-router-dom'
import { Button } from '../../../components/elements/Button'

export const GetMoreUpdates = () => {
  return (
    <div>
       

<div class="w-full bg-white border border-gray-200 mb-3 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
   
    <div id="">
        <div class=" p-4 bg-white rounded-lg md:p-8 dark:bg-gray-800 text-lg" id="about" role="tabpanel" aria-labelledby="about-tab">
            <h2 class="mb-3 text-2xl font-extrabold tracking-tight text-gray-900 dark:text-white">Get More Updates...</h2>
            <p class="mb-3 text-gray-500 dark:text-gray-400">Don't miss out on the latest nutrition trends, expert advice, and community updates. Subscribe to our newsletter and join thousands of others on the journey to optimal health.
            Connect With Us:
Follow us on social media to stay connected between newsletters. Join the conversation, share your thoughts, and be part of our growing community.</p>
        <div className='flex justify-start items-center '>
            
            
<form class="max-w-sm ">
  
  <div class="relative">
    <div class="absolute inset-y-0 start-0 flex items-center ps-3.5 pointer-events-none">
      <svg class="w-4 h-4 text-gray-500 dark:text-gray-400" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 16">
        <path d="m10.036 8.278 9.258-7.79A1.979 1.979 0 0 0 18 0H2A1.987 1.987 0 0 0 .641.541l9.395 7.737Z"/>
        <path d="M11.241 9.817c-.36.275-.801.425-1.255.427-.428 0-.845-.138-1.187-.395L0 2.6V14a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2V2.5l-8.759 7.317Z"/>
      </svg>
    </div>
    <input type="text" id="email-address-icon" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-96 ps-10 p-2.5  h-12 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="name@fitbite.com"/>
  </div>
</form>
<div>
<Link className='ml-4'>

<Button>
Subscribe
 <svg class=" w-2.5 h-2.5 ms-2 rtl:rotate-180" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
     <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 9 4-4-4-4"/>
 </svg>
</Button>
</Link>


</div>

        </div>
        <div className='mt-3  text-lg text-gray-500 dark:text-gray-400'>
  <p>By subscribing, you agree with Fitbite's <Link to="/terms-of-service">
  <span className='text-blue-700'>Terms of Service</span></Link> and 
  <Link to="/privacy-policy">
  <span className='text-blue-700'> Privacy Policy.</span></Link></p>
</div>
            
        </div>
        
       
    </div>
</div>

        
    </div>
  )
}
