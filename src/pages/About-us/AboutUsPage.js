import React from 'react'

import { OurMission } from './components/OurMission'
import { HeroSection } from './components/HeroSection'
import { WhoWeAre } from './components/WhoWeAre'
import { WhatSetsUsApart } from './components/WhatSetsUsApart'

import { GetMoreUpdates } from './components/GetMoreUpdates'

export const AboutUsPage = () => {
  return (
   <>
   
    
   <main>
     
     <div> 
      <HeroSection/>
    
    <OurMission />
    <WhoWeAre />
    <WhatSetsUsApart />
    <GetMoreUpdates />
      
    </div>
   </main>
   
   </>
  )
}
