import React from 'react';
import { Ratings } from './Ratings'; 

export const RestaurantCard = ({ restaurant }) => {
  const {
    name,
    is_open,
    weighted_rating_value,
    cuisines,
    delivery_enabled,
    pickup_enabled,
    logo_photos,
    local_hours,
  } = restaurant;

  const imageSrc = logo_photos ? logo_photos[0] : 'fallback_image_url';

  return (
    <div className="bg-white rounded-md overflow-hidden w-full shadow-md flex mb-4">
      <div className="flex items-center justify-center w-1/3">
        <img className="w-400 h-200 object-cover" src={imageSrc} alt={name} />
      </div>
      <div className="flex-1 p-4">
        <h5 className="text-xl font-bold mb-2">{name}</h5>
        <Ratings rating={weighted_rating_value || 0} />
        <div className={`text-white font-semibold py-1 px-2 rounded-full ${is_open ? 'bg-green-500' : 'bg-red-500'} inline-block mb-2`}>
          {is_open ? 'Open' : 'Closed'}
        </div>
        <p className="text-gray-700 mb-2"><strong>Cuisines:</strong> {cuisines.join(', ')}</p>
        <div className="flex items-center">
          <i className={`bi-truck text-gray-500 mr-2 ${delivery_enabled ? 'text-green-500' : 'text-red-500'}`} />
          <p className="text-gray-700"><strong>Delivery:</strong> {delivery_enabled ? 'Yes' : 'No'}</p>
        </div>
        <div className="flex items-center mt-2">
          <i className={`bi-box text-gray-500 mr-2 ${pickup_enabled ? 'text-green-500' : 'text-red-500'}`} />
          <p className="text-gray-700"><strong>Pickup:</strong> {pickup_enabled ? 'Yes' : 'No'}</p>
        </div>
        <div className="text-gray-700 mt-2">
          <i className="bi-clock text-gray-500 mr-2" />
          <strong>Operational Hours:</strong>
          <p className="ml-2">
            Delivery: {local_hours.delivery.Monday} | Pickup: {local_hours.pickup.Monday}
          </p>
        </div>
      </div>
    </div>
  );
};
