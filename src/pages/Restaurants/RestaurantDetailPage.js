import React from "react";

const RestaurantDetailPage = ({ restaurant }) => {
  const {
    name,
    description,
    cuisines,
    address,
    phone_number,
    weighted_rating_value,
    delivery_enabled,
    pickup_enabled,
    local_hours,
    logo_photos,
    food_photos,
  } = restaurant;

  return (
    <div>
      <div>
        <h1>{name}</h1>
        <p>{description}</p>
        <p>Cuisines: {cuisines.join(", ")}</p>
        <p>Phone Number: {phone_number}</p>
        <p>Address: {`${address.city}, ${address.country} ${address.zipcode}`}</p>
        <p>Weighted Rating: {weighted_rating_value}</p>
      </div>

      <div>
        {/* Display restaurant photos */}
        <div>
          <h2>Restaurant Photos</h2>
          {logo_photos.map((photo, index) => (
            <img key={index} src={photo} alt={`Restaurant Logo ${index + 1}`} />
          ))}
        </div>

        {/* Display food photos */}
        <div>
          <h2>Food Photos</h2>
          {food_photos.map((photo, index) => (
            <img key={index} src={photo} alt={`Food Photo ${index + 1}`} />
          ))}
        </div>
      </div>

      <div>
        {/* Display operational hours for different services */}
        <h2>Operational Hours</h2>
        <div>
          <h3>Delivery Hours</h3>
          {Object.entries(local_hours.delivery).map(([day, hours]) => (
            <p key={day}>{`${day}: ${hours}`}</p>
          ))}
        </div>

        <div>
          <h3>Pickup Hours</h3>
          {Object.entries(local_hours.pickup).map(([day, hours]) => (
            <p key={day}>{`${day}: ${hours}`}</p>
          ))}
        </div>

        <div>
          <h3>Dine-in Hours</h3>
          {Object.entries(local_hours.dine_in).map(([day, hours]) => (
            <p key={day}>{`${day}: ${hours}`}</p>
          ))}
        </div>
      </div>

      <div>
        {/* Display options for delivery and pickup */}
        <h2>Delivery and Pickup Options</h2>
        <p>Delivery Available: {delivery_enabled ? "Yes" : "No"}</p>
        <p>Pickup Available: {pickup_enabled ? "Yes" : "No"}</p>
      </div>
    </div>
  );
};

export default RestaurantDetailPage;
