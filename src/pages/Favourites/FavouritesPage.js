import { useDynamicTitle } from "../../hooks";
import { FavList } from "./components/FavList";
import { FavEmpty } from "./components/FavEmpty";

import { useEffect, useState } from "react";
import { getFavourites } from "../../services/favService";
import { toast } from "react-toastify";


export const FavouritesPage = () => {

  useDynamicTitle(`My favourites | Fitbite}`);

  const [favourites, setFavourites] = useState([]);
    

    useEffect(() => {
      async function fetchFavourites() {
        try {
          const data = await getFavourites();
          setFavourites(data);
        } catch(error) {
          toast.error(error.message, {
            position: toast.POSITION.BOTTOM_RIGHT
          });
        }
      }
    
  
      fetchFavourites();
    }, []);
  return (
    
    
      <main>       
        { favourites.length ? <FavList  favourites = { favourites }/> : <FavEmpty /> }   
      </main>
    
  );
};
