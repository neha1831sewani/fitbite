import { Link } from "react-router-dom"

export const FavEmpty = () => {
  return (
    <section className="text-xl text-center max-w-4xl mx-auto my-10 py-5 dark:text-slate-100 border dark:border-slate-700 rounded">
        <div className="my-5">
            <p className="bi bi-heart text-primary-950 text-7xl mb-5"></p>
            <p>Oops! Your favourites list looks empty!</p>
            <p>Add Recipes to your favourites from our recipes collection.</p>
        </div>
        <Link to="/recipes" type="button" className="text-white bg-blue-600 hover:bg-primary-950 rounded-lg text-lg px-5 py-2.5 mr-2 mb-2 focus:outline-none">Add Recipes<i className="ml-2 bi bi-heart"></i></Link>
    </section>
  )
}
