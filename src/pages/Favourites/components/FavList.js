import { useSelector } from "react-redux";
import { FavCard } from "./FavCard";
import { useEffect, useState } from "react";
import { getfavourites } from "../../../services/favService";
import { toast } from "react-toastify";

export const FavList = ({favourites}) => {
  // console.log(favourites);
  // const { favourites } = useSelector( state => state.fav );
  
  return (
    <>
      <section>
        <p className="text-2xl text-center font-semibold dark:text-slate-100 my-5 underline underline-offset-8">
          My favourites : ({favourites.length})
        </p>
      </section>

      <section>
        {favourites.map((recipe) => (
          <FavCard key={recipe} recipe={recipe} />
        ))}
      </section>
    </>
  );
};
