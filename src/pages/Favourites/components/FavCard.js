
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { removeFromFav } from '../../../store/favSlice';

export const FavCard = ({ recipe }) => {
  const dispatch = useDispatch();

  return (
    <div className="bg-white rounded-md overflow-hidden w-full shadow-lg flex mb-4">
      <div className="flex items-center justify-center w-1/3">
        <img className="w-400 h-200 object-cover rounded p-2" src={recipe?.image} alt={recipe?.title} />
      </div>
      <div className="flex-1 p-4">
        <h5 className="text-xl font-bold mb-2">{recipe?.title}</h5>

        <div className="flex justify-between mt-4">
          <div>
            <p className="text-lg mb-2">
              Want to see detailed recipe?
              <Link to={`/recipes/${recipe?.id}`} className="text-blue-700 decoration-solid underline">
                Click Here
              </Link>
            </p>
          </div>
        </div>

        <div>
          <button
            type="button"
            className="text-red-500 hover:text-red-600 bg-red-200 py-[0.2rem] px-1.5 border-2 mt-4 mr-3 rounded border-red-700 focus:outline-none"
            onClick={() => dispatch(removeFromFav({recipe}))}
          >
            <i className="bi-trash-fill">
              <span className="sr-only">Delete</span>
            </i>
          </button>
        </div>
      </div>
    </div>
  );
};
