import React, { useEffect } from 'react';
import { useSearchParams } from 'react-router-dom';

import { RecipeCard } from './Recipies/components/RecipeCard';
import { useFetch } from '../hooks';

export const SearchPage = () => {
  const [params] = useSearchParams();
  const queryString = params.get('q');
  const API_KEY = process.env.REACT_APP_API_KEY;
  const BASE_URL = process.env.REACT_APP_BASE_URL;

  const { data, isLoading, error, setUrl } = useFetch();

  useEffect(() => {
    const URL = `${BASE_URL}recipes/complexSearch?query=${queryString}&apiKey=${API_KEY}`;
    setUrl(URL);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [queryString]);

  

  return (
    <main>
      <h1 className='text-3xl dark:text-white text-slate-800 mt-3'>
        {data && data.results.length === 0 ? (
          `No recipes found for ${queryString}`
        ) : (
          <>
            Search Results for:{" "}
            <span className="font-bold dark:text-green-500">{queryString}</span>
          </>
        )}
      </h1>
      <div className='flex justify-start flex-wrap '>
        {isLoading}
        {data &&
          data.results.map((recipe) => (
            <RecipeCard key={recipe.id} recipe={recipe} queryString={queryString}/>
          ))}
      </div>
    </main>
  );
};
