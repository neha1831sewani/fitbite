import React, { useEffect, useState } from "react";
import { useDynamicTitle, useFetch } from "../../hooks";
import { RecipeCard} from "./components/RecipeCard";


export const RecipesPage = () => {
  useDynamicTitle("All recipes | Fitbite", "Fitbite");

  const BASE_URL = process.env.REACT_APP_BASE_URL;
  const API_KEY = process.env.REACT_APP_API_KEY;
  const { data, isLoading, error, setUrl } = useFetch();

  const [visibleRecipes, setVisibleRecipes] = useState(9);


  useEffect(() => {
    const initialUrl = `${BASE_URL}recipes/complexSearch?apiKey=${API_KEY}&offset=0&number=${visibleRecipes}`;
    setUrl(initialUrl);
  }, [setUrl, BASE_URL, API_KEY, visibleRecipes]);

  const loadMore = () => {
    
    setVisibleRecipes((prevVisibleRecipes) => prevVisibleRecipes + 9);
  };

  
  const recipes = data?.results;

  return (
    <main>
      <section className="my-5">
      <h2 className="text-2xl font-bold">Total Recipes: <span className="text-green-500">{data?.totalResults}</span></h2>
          <div className="flex flex-wrap justify-start lg:flex-row">
           
            {recipes &&
              recipes.map((recipe) => (
                <RecipeCard key={recipe.id} recipe={recipe} />
              ))}
          </div>
        

        {visibleRecipes < (data?.totalResults || 0) && (
  <div className="flex justify-center mt-4">
    <button
      onClick={loadMore}
      className=" bg-blue-500 hover:bg-primary-950 text-white px-4 py-2 rounded "
    >
      Load More
    </button>
  </div>
)}
</section>
 
</main>


     
  );
};
