import React from 'react';
import { Link, useParams } from 'react-router-dom';
import { useFetch } from '../../hooks';
import timer from '../../assets/timer.png';
import heart from '../../assets/heart.png';
import dollar from '../../assets/money-bag.png';
import score from '../../assets/good-feedback.png';
import glutenfree from '../../assets/gluten-free.png';
import dairyfree from '../../assets/dairy-free.png';
import vegan from '../../assets/vegan.png';
import vegetarian from '../../assets/veg-icon.webp';
import nonvegetarian from '../../assets/non-veg-icon.webp';
import { SimilarRecipes } from './components/SimilarRecipes';



export const RecipeDetailsPage = () => {
  const params = useParams();
  const recipeId = params.id;
  const BASE_URL = process.env.REACT_APP_BASE_URL;
  const API_KEY = process.env.REACT_APP_API_KEY;

  const { data: recipe } = useFetch(`${BASE_URL}recipes/${recipeId}/information?apiKey=${API_KEY}`);
  // console.log(recipe)
  // console.log(recipe);
  

  const renderInstructions = () => {
    if (recipe?.instructions) {
      const instructionsArray = recipe.instructions.split(/\.\s/);

      return (
        <ol className="text-left mt-1">
          {instructionsArray.map((instruction, index) => (
            <li key={index}> {index + 1}. {instruction}</li>
            //index + 1 bcz index of array starts from 0 toh instructions were displaying from 0
          ))}
        </ol>
      );
    }

    return null;
  };

  const renderIngredients = () => {
    return (
      <div className="flex flex-wrap justify-center items-center mt-6">
        {recipe?.extendedIngredients && recipe.extendedIngredients.map((ingredient) => (
          <div key={ingredient.id} className="flex-col mr-10 items-center">
            <img src={`https://spoonacular.com/cdn/ingredients_100x100/${ingredient.image}`} alt={ingredient.name} className="h-12 w-12 mx-auto mt-2" />
            <p className='text-center'>{ingredient.amount} {ingredient.unit}</p>
            <p className='text-center'>{ingredient.name}</p>
          </div>
        ))}
      </div>
    );
  };

  return (
    <div className="container mx-auto max-w-screen-lg ">
     <div className=" mt-3 text-gray-800 dark:text-white">
     <Link to="/recipes" >
        <svg className="w-6 h-6 mr-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 10">
          <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M13 5H1m0 0 4 4M1 5l4-4" />
        </svg>
        Back
      </Link>

     </div>
      <section className="flex flex-col items-center dark:text-white">
        
       
     
        <div className=" flex justify-center items-center ">
       
          <div className="">
          <h1 className="font-medium mt-2 hover:underline text-3xl">{recipe?.title}</h1>

          <p className='text-lg text-gray-500 mt-2'> <span className='text-red-500'>{recipe?.aggregateLikes} Likes</span>  | <span className='text-green-500'>{recipe?.spoonacularScore.toFixed(2)}% Fitbite Score </span> | <span className='text-blue-500'>Ready in {recipe?.readyInMinutes} minutes</span> | <span className='text-orange-500'>{recipe?.pricePerServing} $ per serving</span></p>
         
          <div  className= " mt-3" dangerouslySetInnerHTML={{ __html: recipe?.summary }} />
            
            <div className="flex justify-center items-center mt-6">
            <img src={recipe?.image} alt={recipe?.title} className=" mt-2 rounded-lg shadow-md" />
            <div className="flex-col ml-8 items-start">
            {recipe?.vegetarian === true && (
              <div className="flex items-center mt-4 ">
                <img src={vegetarian} alt="Vegetarian" className="h-12 w-12 mx-auto" />
               
              </div>
            )}
            {recipe?.glutenFree === true && (
              <div className="flex items-center mt-4 ">
                <img src={glutenfree} alt="Gluten-free" className="h-12 w-12 mx-auto" />
              
              </div>
            )}
            {recipe?.dairyFree === true && (
              <div className="flex items-center mt-4 ">
                <img src={dairyfree} alt="Dairy-free" className="h-12 w-12 mx-auto" />
               
              </div>
            )}
            {recipe?.vegan === true && (
              <div className="flex items-center mt-4 ">
                <img src={vegan} alt="Vegan" className="h-12 w-12 mx-auto" />
                
              </div>
            )}
            {recipe?.vegetarian === false && (
              <div className="flex items-center mt-4 ">
                <img src={nonvegetarian} alt="Non-Vegetarian" className="h-14 w-12 mx-auto" />
             
              </div>
            )}
          </div>
            </div>
           
          </div>
          
        </div>

        <div className="flex justify-center items-center mt-6">
          <div className="flex-col items-center">
            <img src={heart} alt="" className="h-12 w-12 " />
            <p className="text-center"> {recipe?.aggregateLikes} likes</p>
          </div>
          <div className="flex-col items-center ml-6">
            <img src={timer} alt="" className="h-12 w-12 mx-auto" />
            <p className="text-center">Ready in {recipe?.readyInMinutes} minutes</p>
          </div>
          <div className="flex-col items-center ml-6">
            <img src={dollar} alt="" className="h-12 w-12 mx-auto" />
            <p className="text-center"> {recipe?.pricePerServing} $ per serving</p>
          </div>
          <div className="flex-col items-center ml-6">
            <img src={score} alt="" className="h-12 w-12 mx-auto" />
            <p className="text-center">Fitbite score {recipe?.spoonacularScore.toFixed(2)}%</p>
          </div>
        </div>

        <div className="text-justify mt-6">
          <h2 className="text-xl font-bold mb-4">Ingredients Required:</h2>
          {renderIngredients()}
        </div>

        <div className="text-justify mt-6">
          <h2 className="text-xl font-bold mb-4">Directions:</h2>
          <p className="text-lg">{renderInstructions()}</p>
        </div>

       
       

        {recipe?.id && <SimilarRecipes 
        id = {recipe.id}
      />}
      
        
     

      </section>

     
      
    </div>
  );
};

