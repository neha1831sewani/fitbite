import React, { useEffect } from "react";
import { useFetch } from "../../../hooks";
import { RecipeCard } from "./RecipeCard";

export const SimilarRecipes = ({ id }) => {
  const { data, error, loading, setUrl } = useFetch();
  const BASE_URL = process.env.REACT_APP_BASE_URL;
  const API_KEY = process.env.REACT_APP_API_KEY;

  //   https://api.spoonacular.com/recipes/{id}/similar

  useEffect(() => {
    
    const initialUrl = `${BASE_URL}recipes/${id}/similar?apiKey=${API_KEY}`;

    setUrl(initialUrl);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);
//   console.log(data);
  if (id) {
    return (
        <main>
        <section className="my-5">
        <div className="text-justify mt-6">
          <h2 className="text-xl font-bold mb-4">Similar Recipes:</h2>
        
        </div>
  
            <div className="flex flex-wrap justify-start lg:flex-row">
              {data &&
                data.map((recipe) => (
                  <RecipeCard key={recipe.id} recipe={recipe} />
                ))}
            </div>
        
  
   
  
  </section>

  </main>
  
    );
    
  }
};
