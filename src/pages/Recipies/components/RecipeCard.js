import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { addToFav, removeFromFav } from "../../../store/favSlice";
import { getSessionData } from "../../../services/authservices";
import { addFavourites, getFavourites, removeFromFavourites } from "../../../services/favService";
import { useAddToFavourites } from "../../../hooks/useAddToFavourites";

export const RecipeCard = ({ recipe, queryString }) => {
  const { title } = recipe;
  
  const dispatch = useDispatch();
  const { favouritesList } = useSelector(state => state.fav);
  const [inFavourites, setInFavourites] = useState(false);
  // const userInfo = getSessionData();
  const browserData = getSessionData();
  const addFavourites = useAddToFavourites()
  const [favToggl, setFavToggl] = useState(true);

  const highlightQueryInTitle = (title, queryString) => {
    if (!title || !queryString) {
      return title;
    }

    const lowerCaseTitle = title.toLowerCase();
    const lowerCaseQuery = queryString.toLowerCase();

    const parts = [];
    let startIndex = 0;

    while (startIndex < lowerCaseTitle.length) {
      const queryIndex = lowerCaseTitle.indexOf(lowerCaseQuery, startIndex);

      if (queryIndex === -1) {
        parts.push(title.substring(startIndex));
        break;
      }

      parts.push(title.substring(startIndex, queryIndex));

      parts.push(
        <span key={startIndex} className="font-bold text-blue-700 dark:text-green-500">
          {title.substring(queryIndex, queryIndex + queryString.length)}
        </span>
      );

      startIndex = queryIndex + queryString.length;
    }

    return parts;
  };

  
  useEffect(() => {
    // console.log("Recipe ID:", recipe.id);
    // console.log("Favourites List:", favouritesList);

    (async ()=> {
      const list = await getFavourites();
      // console.log(list);
      // if(list.length === 0){
      //   return;
      // }
      console.log("List:" , list);
      const recipeInFavourites = list.length === 0 ? undefined :  list.find(item => item.id === recipe.id);
      console.log("Recipe in Favourites:", recipeInFavourites);
      if (recipeInFavourites !== undefined) {
        setInFavourites(true);
      } else {
        setInFavourites(false);
      }
    })();
    
   
  
   
  }, [favToggl]);
  
  
  


  const handleAddToFav = () => {
    if (browserData.token) {
      // dispatch(addToFav({ recipe }));
      

      
      addFavourites(recipe, browserData.userId);
      setFavToggl(!favToggl);
      // addFavourites(recipe.id, userInfo.userId);
    } else {
      
      alert("User is not logged in");
    }
  };

  const handleRemoveFromFav = () => {
    if (browserData.token) {
      // dispatch(removeFromFav({ recipe }));
      removeFromFavourites(recipe.id,browserData.userId);
      setInFavourites(false);
    

    } else {
     
      alert("User is not logged in");
    }
  };

  
  
  
  return (
    <div className="m-2 w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
      <Link to={`/recipes/${recipe.id}`}>
        <img className="p-8 rounded-t-lg" src={ `https://spoonacular.com/recipeImages/${recipe.id}-556x370.jpg`} alt={recipe.title} />
      </Link>
      <div className="px-5 pb-5 flex items-center justify-between">
        <Link to={`/recipes/${recipe.id}`}>
          <h5 className="text-lg font-semibold tracking-tight text-gray-900 hover:underline dark:text-white">
            {highlightQueryInTitle(title, queryString)}
          </h5>
        </Link>
        <Link to="#"   >

        
          {!inFavourites ? (
            <i className="bi bi-heart font-bold text-xl" onClick={ handleAddToFav }></i>
          ) : (
            <i className="bi bi-heart-fill text-red-500 dark:text-white font-bold text-lg"   onClick={handleRemoveFromFav}></i>
          )}
        </Link>
      </div>
    </div>
  );
};
