/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";

export const Pagination = ({ totalPages, onPageChange, currentPage, visibleRecipes }) => {
  const renderPageNumbers = () => {
    const visiblePageCount = 10;
    const halfVisibleCount = Math.floor(visiblePageCount / 2);

    if (totalPages <= visiblePageCount) {
      return Array.from({ length: totalPages }, (_, index) => (
        <li key={index + 1}>
          <a
            href="#"
            className={`flex items-center justify-center px-4 h-10 leading-tight ${
              currentPage === index + 1
                ? "text-blue-600 border-blue-500 bg-blue-200 hover:bg-blue-100 hover:text-blue-700 dark:border-gray-700 dark:bg-gray-700 dark:text-white"
                : "text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"
            }`}
            onClick={(evt) => {
              evt.preventDefault();
              onPageChange(index + 1);
            }}
          >
            {index + 1}
          </a>
        </li>
      ));
    } else {
      let startPage = Math.max(1, currentPage - halfVisibleCount);
      let endPage = Math.min(totalPages, startPage + visiblePageCount - 1);

      if (endPage - startPage < visiblePageCount - 1) {
        startPage = Math.max(1, endPage - visiblePageCount + 1);
      }

      return Array.from({ length: endPage - startPage + 1 }, (_, index) => (
        <li key={startPage + index}>
          <a
            href="#"
            className={`flex items-center justify-center px-4 h-10 leading-tight ${
              currentPage === startPage + index
                ? "text-blue-600 border  border-gray-300 bg-blue-100 hover:bg-primary-950 hover:text-blue-700 dark:border-gray-700 dark:bg-gray-700 dark:text-white"
                : "text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white"
            }`}
            onClick={(evt) => {
              evt.preventDefault();
              onPageChange(startPage + index);
            }}
          >
            {startPage + index}
          </a>
        </li>
      ));
    }
  };

  return (
    <div className="flex justify-center items-center mt-8">
      <div aria-label="Page navigation example  " className="mx-auto ">
        <ul className="flex items-center h-10 text-base mx-auto">
          <li>
            <a
              href="#"
              className={`flex items-center justify-center px-4 h-10 ms-0 leading-tight text-gray-500 bg-white border border-e-0 border-gray-300 rounded-s-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white`}
              onClick={(evt) => {
                evt.preventDefault();
                onPageChange(currentPage - 1);
              }}
            >
              <span className="sr-only">Previous</span>
              <svg
                className="w-3 h-3 rtl:rotate-180"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 6 10"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M5 1 1 5l4 4"
                />
              </svg>
            </a>
          </li>
          {renderPageNumbers()}
          <li>
            <a
              href="#"
              className={`flex items-center justify-center px-4 h-10 leading-tight text-gray-500 bg-white border border-gray-300 rounded-e-lg hover:bg-gray-100 hover:text-gray-700 dark:bg-gray-800 dark:border-gray-700 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white`}
              onClick={(evt) => {
                evt.preventDefault();
                onPageChange(currentPage + 1);
              }}
            >
              <span className="sr-only">Next</span>
              <svg
                className="w-3 h-3 rtl:rotate-180"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 6 10"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="m1 9 4-4-4-4"
                />
              </svg>
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};
