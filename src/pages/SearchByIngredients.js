import React, { useEffect } from 'react';
import { useSearchParams } from 'react-router-dom';

import { useFetch } from '../hooks';
import { RecipeCard } from './Recipies/components/RecipeCard';

export const SearchByIngredients = () => {
  const [params] = useSearchParams();
  const queryString = params.get('q');
  const API_KEY = process.env.REACT_APP_API_KEY;
  const BASE_URL = process.env.REACT_APP_BASE_URL;

  const { data, isLoading, error, setUrl } = useFetch();

  useEffect(() => {
    const URL = `${BASE_URL}recipes/findByIngredients?ingredients=?query=${queryString}&apiKey=${API_KEY}`;
    setUrl(URL);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [queryString]);

  console.log(data);



  

  return (
    <main>
      <h1 className='text-3xl dark:text-white text-slate-800 mt-3'>
        {data && data.length === 0 ? (
          `No recipes found for ${queryString}`
        ) : (
          <>
            Search Results for:{" "}
            <span className="font-bold dark:text-green-500">{queryString}</span>
          </>
        )}
      </h1>
      <div className='flex justify-start flex-wrap '>
        {isLoading}
        {data &&
          data.map((recipe) => (
            <RecipeCard key={recipe.id} recipe={recipe} queryString={queryString}/>
          ))}
      </div>
    </main>
  );
};
