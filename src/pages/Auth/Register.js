import { toast } from "react-toastify";
import { useDynamicTitle } from "../../hooks";
import { getUserRegister } from "../../services/authservices";
import { Link, useNavigate } from "react-router-dom";
import Cookies from "js-cookie";

export const Register = () => {
  useDynamicTitle("Register");

  const navigate = useNavigate();
  const handleSubmit = async (evt) => {
    evt.preventDefault();

    const name = evt.target.name.value;
    const email = evt.target.email.value;
    const password = evt.target.password.value;
    const repeatPassword = evt.target.repeatpassword.value;

    if (password === repeatPassword) {
      try {
        const data = await getUserRegister({ name, email, password });
        navigate("/login");
        toast.success("Registration successful!", {
          position: "top-center",
          autoClose: 2000,
        });
      } catch (error) {
        const errorObj = JSON.parse(error.message);
        toast.error(errorObj.message, {
          position: "top-center",
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
        });
      }
    } else {
      evt.target.repeatpassword.value = "";
      toast.error("Passwords do not match. Please re-enter your password.", {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
      });
    }
  };

  return (
    <main className="flex flex-col items-center mt-7">
      <section>
        <p className="text-2xl text-center font-semibold dark:text-slate-100 my-5 underline underline-offset-8">
          Register
        </p>
      </section>
      <form className="max-w-md w-full bg-white shadow-md rounded-lg px-8 pt-6 pb-8 mb-4" onSubmit={handleSubmit}>
        <div className="mb-4">
          <label htmlFor="name" className="block text-gray-700 text-sm font-bold mb-2">
            Your Name
          </label>
          <input
            type="name"
            id="name"
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            placeholder="Your Name"
            name="name"
            required
          />
        </div>
        <div className="mb-4">
          <label htmlFor="email" className="block text-gray-700 text-sm font-bold mb-2">
            Your Email
          </label>
          <input
            type="email"
            id="email"
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
            placeholder="name@flowbite.com"
            name="email"
            required
          />
        </div>
        <div className="mb-4">
          <label htmlFor="password" className="block text-gray-700 text-sm font-bold mb-2">
            Create Password
          </label>
          <input
            type="password"
            id="password"
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
            placeholder="Create Password"
            name="password"
            required
          />
        </div>
        <div className="mb-6">
          <label htmlFor="repeat-password" className="block text-gray-700 text-sm font-bold mb-2">
            Repeat Password
          </label>
          <input
            type="password"
            id="repeat-password"
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
            placeholder="Repeat Password"
            name="repeatpassword"
            required
          />
        </div>
        <div className="flex items-center mb-6">
          <input
            type="checkbox"
            id="terms"
            className="mr-2 leading-tight"
            required
          />
          <label htmlFor="terms" className="text-sm text-gray-700">
            I agree with the{" "}
            <Link to="/privacy-policy" className="text-blue-500 hover:underline">
              terms and conditions
            </Link>
          </label>
        </div>
        <button
          type="submit"
          className="w-full bg-blue-700 hover:opacity-30 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
        >
          Register New Account
        </button>
      </form>
    </main>
  );
};
