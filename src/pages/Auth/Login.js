import { useEffect, useRef } from "react";
import { useDynamicTitle } from "../../hooks";
import { getUserLogin } from "../../services/authservices";
import { Link, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import Cookies from "js-cookie";

export const Login = () => {
  useDynamicTitle("Login");
  const emailRef = useRef();
  const passwordRef = useRef();

  const navigate = useNavigate();

  useEffect(() => {
    const token = Cookies.get("accessToken");
   
    if (token) {
      navigate("/");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  async function handleLogin(event) {
    event.preventDefault();
    
    const authDetails = {
      email: emailRef.current.value,
      password: passwordRef.current.value,
    };

    try {
      const data = await getUserLogin(authDetails);
      Cookies.set("name", data.user.name);
      Cookies.set("email", data.user.email);
      Cookies.set("accessToken", data.accessToken);
      Cookies.set("id", data.user.id);
      
      navigate("/");
      toast.success("Login successful!", {
        position: "top-center",
      });
    } catch (error) {
      const errorObj = JSON.parse(error.message);
      if (error.message && errorObj.status === 400) {
        // Unauthorized (incorrect password)
        toast.error("Incorrect email or password", {
          position: "top-center",
          autoClose: 2000,
        });
      } else {
        toast.error(errorObj.message, {
          position: "top-center",
          autoClose: 2000,
        });
      }
    }
  }
  
  return (
    <main className="flex flex-col items-center mt-7 ">
      <section className="text-center mb-8">
        <h2 className="text-2xl font-semibold text-black dark:text-primary-300">Login to Your Account</h2>
      </section>
      <form className="max-w-md w-full bg-white shadow-md rounded-lg px-8 pt-6 pb-8 mb-4" onSubmit={handleLogin}>
        <div className="mb-4">
          <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="email">
            Email Address
          </label>
          <input
            ref={emailRef}
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            id="email"
            type="email"
            placeholder="Enter your email"
            required
          />
        </div>
        <div className="mb-6">
          <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">
            Password
          </label>
          <input
            ref={passwordRef}
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
            id="password"
            type="password"
            placeholder="Enter your password"
            required
          />
        </div>
        <Link to="/register" className="text-blue-500 dark:text-primary-300 font-semibold hover:underline">Create an Account</Link>
        <button
          type="submit"
          className="w-full bg-blue-700 hover:opacity-30 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
        >
          Login
        </button>
      </form>
    </main>
  );
};
