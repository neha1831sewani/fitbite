import React from 'react';
import { useDynamicTitle } from '../hooks';

export const TermsOfServicePage = () => {
  useDynamicTitle("Terms of Service | FitBite");

  return (
    <main className="terms-of-service-container">
      <div className="terms-of-service-content dark:text-slate-200">
        <h2 className="h2 text-2xl mt-3">Terms of Service</h2>
        <p className="mb-4">
          Last Updated: November 24, 2023
        </p>
        <p className="mb-4">
          Welcome to <strong>FitBite!</strong> These terms of service outline the rules and regulations for the use of our recipe and nutrition website.
        </p>
        <h3 className="h3 text-lg">1. Acceptance of Terms</h3>
        <p className="mb-4">
          By accessing and using FitBite, you accept and agree to be bound by the terms and conditions outlined in these Terms of Service.
        </p>
        <h3 className="h3 text-lg">2. User Conduct</h3>
        <p className="mb-4">
          Users agree to abide by all applicable local, state, national, and international laws and regulations when using our recipe and nutrition website. Additionally, users agree not to:
        </p>
        <ul className="mb-4 list-disc ml-3">
          <li>Violate any laws or regulations;</li>
          <li>Interfere with the functionality of the website;</li>
          <li>Engage in any form of harassment, spam, or unauthorized advertising;</li>
          <li>Impersonate another person or entity;</li>
          <li>Collect or store personal information about other users without their consent;</li>
          <li>Attempt to gain unauthorized access to the website's systems or networks.</li>
        </ul>
        <h3 className="h3 text-lg">3. Intellectual Property</h3>
        <p className="mb-4">
          All content on FitBite, including recipes, text, graphics, logos, and images, is the property of FitBite and is protected by copyright laws. Users may not use, reproduce, or distribute the content without prior written permission.
        </p>
        <h3 className="h3 text-lg">4. Disclaimer</h3>
        <p className="mb-4">
          FitBite makes no warranties or representations about the accuracy or completeness of the recipes or content on the website. Use the information at your own risk.
        </p>
        <h3 className="h3 text-lg">5. Changes to Terms</h3>
        <p className="mb-4">
          FitBite reserves the right to modify these Terms of Service at any time. Changes will be effective immediately upon posting. Users are encouraged to review the terms regularly to stay informed about updates.
        </p>
        <h3 className="h3 text-lg">6. Contact Information</h3>
        <p className="mb-4">
          If you have any questions about these Terms of Service, please contact us at support@fitbite.com.
        </p>
      </div>
    </main>
  );
};

