import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  favouritesList: [],
  
};

export const favSlice = createSlice({
  name: 'fav',
  initialState,
  reducers: {
    addToFav: function(state, action) {
      
      return {
        ...state,
         favouritesList: state.favouritesList.concat(action.payload.recipeId),
     
      };
      
    },
    removeFromFav: function(state, action) {
      
      
      return {
        ...state,
         favouritesList: state.favouritesList.filter(recipe => recipe.id !== action.payload.recipe.id),
       
      };
    },

    clearFav: function(state, action) {
      return initialState;
    }
  }
  
});

export const { addToFav, removeFromFav, clearFav } = favSlice.actions;

const favReducer = favSlice.reducer;
export default favReducer;
